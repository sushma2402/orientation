## Git Basics

### What is Git?
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.

![git](https://www.nobledesktop.com/image/blog/git-branches-merge.png)

### Git Repositories
A Git repository (or repo for short) contains all of the project files and the entire revision history. You’ll take an ordinary folder of files (such as a website’s root folder), and tell Git to make it a repository. This creates a .git subfolder, which contains all of the Git metadata for tracking changes.


### Remote Repositories

![remote repositories](https://www.nobledesktop.com/image/blog/git-distributed-workflow-diagram.png)

### Operations & Commands
Some of the basic operations in Git are:
* Initialize
* Add
* Commit
* Pull
* Push
Some advanced Git operations are:
* Branching
* Merging
* Rebasing

![commands](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/11/Git-Architechture-Git-Tutorial-Edureka-2-768x720.png)
